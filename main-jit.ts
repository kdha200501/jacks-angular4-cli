import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './<%= __rootAppDir %>/AppModule';

// platformBrowserDynamic - ships the Angular compiler to the browser to dynamically compile the application
platformBrowserDynamic().bootstrapModule(AppModule);
