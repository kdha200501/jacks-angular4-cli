/*
global require, module
*/

// Global methods
var parseUrl = require('url').parse;
var dirname = require('path').dirname;
var joinPath = require('path').join;

// Global Classes
var NodePackageService = require('./NodePackageService');

// Global variables
var DIR = require('./GulpConst').DIR;
var TASK = require('./GulpConst').TASK;

// maps CommonJs packages' import path to file path
var CommonJsFactory = function() {// closure
  'use strict';

  // private variables
  var moduleList = [{// client browser dependencies
    name: '@angular/core',
    path: '@angular/core/bundles/core.umd.min.js'
  }, {
    name: '@angular/common',
    path: '@angular/common/bundles/common.umd.min.js'
  }, {
    name: '@angular/compiler',
    path: '@angular/compiler/bundles/compiler.umd.min.js'
  }, {
    name: '@angular/platform-browser',
    path: '@angular/platform-browser/bundles/platform-browser.umd.min.js'
  }, {
    name: '@angular/platform-browser-dynamic',
    path: '@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.min.js'
  }, {
    name: '@angular/http',
    path: '@angular/http/bundles/http.umd.min.js'
  }, {
    name: '@angular/router',
    path: '@angular/router/bundles/router.umd.min.js'
  }, {
    name: '@angular/router/testing',
    path: '@angular/router/bundles/router-testing.umd.js'
  }, {
    name: '@angular/forms',
    path: '@angular/forms/bundles/forms.umd.min.js'
  }, {
    name: 'rxjs',
    path: 'rxjs'
  }, {
    name: 'lodash',
    path: 'lodash/lodash.min.js'
  }, {
    name: 'angular-webstorage-service',
    path: 'angular-webstorage-service/bundles/angular-webstorage-service.umd.js'
  }, {
    name: 'jacks-angular4-test-utils',
    path: 'jacks-angular4-test-utils'
  }, {
    name: 'mock-reponse',
    path: 'mock-reponse'
  }];

  var testModuleList = [{// Karma browser dependencies
    name: '@angular/core/testing',
    path: '@angular/core/bundles/core-testing.umd.js'
  }, {
    name: '@angular/common/testing',
    path: '@angular/common/bundles/common-testing.umd.js'
  }, {
    name: '@angular/compiler/testing',
    path: '@angular/compiler/bundles/compiler-testing.umd.js'
  }, {
    name: '@angular/platform-browser/testing',
    path: '@angular/platform-browser/bundles/platform-browser-testing.umd.js'
  }, {
    name: '@angular/platform-browser-dynamic/testing',
    path: '@angular/platform-browser-dynamic/bundles/platform-browser-dynamic-testing.umd.js'
  }];

  // public methods
  return {
    instantiate: function(task) {
      switch (task) {
        case TASK.BUILD:

          return moduleList;

          break;
        case TASK.UNIT_TEST:

          return [].concat(moduleList, testModuleList);

          break;
        default:

          return [];
      }
    },
    resolvePath: function(op, module) {
      op[module.name] = [DIR.NODE_MODULES, module.path].join('/');
      return op;
    },
    getPathGlob: function(op, module) {
      var path = joinPath.apply( null, parseUrl(module.path).pathname.split('/') );
      op.push( [DIR.NODE_MODULES, dirname(path), '*.*'].join('/') );
      return op;
    }
  };
}();// endClosure

module.exports = CommonJsFactory;
