/*
global require, module
*/

// Classes
var TASK = require('./GulpConst').TASK;
var NodePackageService = require('./NodePackageService');

// Global variables
var nodePackageService = NodePackageService.getInstance();
var $1 = nodePackageService.getArgs()._[0];// bash $1

var BUILD = {
  LAZYLOAD_MODULE: []
};

var URL = {
  BUILD: nodePackageService.getArgs().url
};

module.exports = {
  $1: $1 === undefined ? TASK.BUILD : $1,// default gulp to build task
  BUILD: BUILD,
  URL: URL
};
