/*
global require, module
*/

// Global methods
var safeAccess = require('safe-access');
var chainTask = require('gulp').series;
var convergeTask = require('gulp').parallel;

// Global classes
var GulpTaskService = require('./GulpTaskService');
var NodePackageService = require('./NodePackageService');

// Global variables
var TASK = require('./GulpConst').TASK;
var ENV = require('./GulpConst').ENV;
var $1 = require('./GulpArgv').$1;

var OrchestrationService = function() {// closure
  'use strict';

  // private static variables and methods
  GulpTaskService.getInstance();// register gulp tasks if not already registered
  var nodePackageService = NodePackageService.getInstance();
  var orchestrationCollection = {};// structure: orchestrationCollection[$1][--env]

  orchestrationCollection[TASK.BUILD] = {};
  orchestrationCollection[TASK.BUILD][ENV.DEV] = chainTask(
    'distRmRF',
    convergeTask(
      'distMkdirP',
      'tsLint'
    ),
    convergeTask(
      'injectEnvConst',
      'injectConfigConst',
      'compileJit',
      'copySystemjsConfig',
      'compileSass',
      'copyIndexHtmlJit',
      'linkView',
      'linkNodeModules',
      'linkMockServer'
    ),
    convergeTask(
      'copyMainJit',
      'compileEnvConst'
    ),
    'compileConfigConst',
    nodePackageService.getArgs().serve === true ? chainTask(
      'startServer',
      convergeTask(
        'watchSource',
        'watchSass',
        'watchView',
        'watchConstant',
        'watchMockServerRouteApi'
      )
    ) : 'noop'
  );
  orchestrationCollection[TASK.BUILD][ENV.STAGE] = chainTask(
    'distRmRF',
    convergeTask(
      'distMkdirP',
      'tsLint'
    ),
    convergeTask(
      'injectEnvConst',
      'injectConfigConst',
      'compileSass',
      'sanitizeView',
      'copyTsConfigAot',
      'copySource',
      'copyWebpackConfig'
    ),
    convergeTask(
      'linkCssRecursive',
      'linkViewRecursive',
      'copyMainAot',
      'renameLazyloadModule'
    ),
    'cleanLazyloadModule',
    'compileAot',
    'codeSplit',
    'copyIndexHtmlAot',
    convergeTask(
      'distCleanAot',
      nodePackageService.getArgs().serve === true ? 'startServer' : 'noop'
    )
  );
  orchestrationCollection[TASK.BUILD][ENV.PROD] = orchestrationCollection[TASK.BUILD][ENV.STAGE];

  orchestrationCollection[TASK.BUILD][ENV.LIB] = chainTask(
    'distRmRF',
    convergeTask(
      'distMkdirP',
      'tsLint'
    ),
    convergeTask(
      'copySource',
      'copyTsConfigMetadata',
      'linkNodeModules'
    ),
    'compileAot',
    'compileLibrary',
    'distCleanLibrary'
  );

  orchestrationCollection[TASK.UNIT_TEST] = {};
  orchestrationCollection[TASK.UNIT_TEST][ENV.DEV] = chainTask(
    'distRmRF',
    convergeTask(
      'distMkdirP',
      'tsLint'
    ),
    convergeTask(
      'injectEnvConst',
      'injectConfigConst',
      'compileJit',
      'copyKarmaConfig',
      'compileSass',
      'linkView',
      'linkNodeModules',
      'linkMockServer',
      'stopUnitTestServer'
    ),
    'compileUnitTest',
    'startUnitTestServer'
  );

  orchestrationCollection[TASK.DOCUMENTATION] = {};
  orchestrationCollection[TASK.DOCUMENTATION][ENV.DEV] = chainTask(
    'docRmRF',
    'docMkdirP',
    'generateDocumentation'
  );

  var getType = function(_key) {// check agaist white-list of types
    var type = null;
    var orchestrationCollection = this;
    for(var key in orchestrationCollection) {
      if( orchestrationCollection.hasOwnProperty(key) && key === _key ) {
        type = key;
        break;
      }
    }
    return type;
  };

  var getEnv = function(config) {// check agaist white-list of environments
    var env = null;
    var orchestration = this;
    if(orchestration) {
      for(var key in orchestration) {
        if( orchestration.hasOwnProperty(key) && config[key] === true ) {
          env = key;
          break;
        }
      }
    }
    return env;
  };

  // private static class
  var singleton = null;
  var OrchestrationService_ = function(config) {
    return function(type, env, orchestrationCollection) {// closure
      
      // public methods
      return {
        getTaskLauncher: function() {
          return safeAccess( orchestrationCollection, [type, env].join('.') );
        }
      };

    }(config.type, config.env, config.orchestrationCollection);// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        var type = getType.call(orchestrationCollection, $1);
        singleton = new OrchestrationService_({
          type: type,
          env: getEnv.call( orchestrationCollection[type], nodePackageService.getArgs() ),
          orchestrationCollection: orchestrationCollection
        });
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = OrchestrationService;
