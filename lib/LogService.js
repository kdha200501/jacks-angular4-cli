/*
global require, module
*/

// Global methods
var timestamp = require('time-stamp');
var chalk = require('chalk');

var paintTimestamp = function() {
  var now = timestamp('HH:mm:ss.ms');
  return ['[', chalk.grey(now), ']'].join('');
};

var paintNamespace = function(namespace) {
  var paint = this;
  return ['[', paint(namespace), ']'].join('');
};

var info = function () {
  var namespace = this;
  var paint = chalk.cyan;
  console.log.apply(
    null,
    [paintTimestamp(), paintNamespace.call(paint, namespace)].concat( Array.prototype.slice.call(arguments) )
  );
};

var warn = function () {
  var namespace = this;
  var paint = chalk.yellow;
  console.log.apply(
    null,
    [paintTimestamp(), paintNamespace.call(paint, namespace)].concat( Array.prototype.slice.call(arguments) )
  );
};

var error = function () {
  var namespace = this;
  var paint = chalk.red;
  console.log.apply(
    null,
    [paintTimestamp(), paintNamespace.call(paint, namespace)].concat( Array.prototype.slice.call(arguments) )
  );
};

module.exports = {
  info: info,
  warn: warn,
  error: error
};
