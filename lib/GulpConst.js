/*
global require, process, module
*/

// Global methods
var joinPath = require('path').join;

// Global Classes
var NodePackageService = require('./NodePackageService');

var DIR = {
  CWD: process.cwd(),
  DIST: 'dist',// build
  DOC: 'doc',// documentation
  TRANSLATIONS: 'i18n',
  MOCK_SERVER: 'mock-server',
  ROUTE_API: joinPath('routes', 'api'),// unit test cases
  NODE_MODULES: 'node_modules',
  UNIT_TEST: 'unit-test',// type script unit test specs
  SRC: 'src',// type script source code
  VIEW: 'view',
  SASS: 'sass',// sass source code
  ROOT_APP: 'root-app',// root application
  CSS: 'css',// compiled sass
  LIB: 'lib'// compiled sass
};

var TASK = {
  DEFAULT: 'default',
  BUILD: 'build',
  UNIT_TEST: 'test',
  DOCUMENTATION: 'doc'
};

var UNIT_TEST_TOKENS = [];
var tokenCollection = require('jacks-angular4-test-utils').Config.TOKEN;
for(var key in tokenCollection) {
  if( tokenCollection.hasOwnProperty(key) ) {
    UNIT_TEST_TOKENS.push(tokenCollection[key]);
  }
}

var ENV = {
  DEV: 'dev',
  STAGE: 'stage',
  PROD: 'prod',
  DRP: 'drp',
  LIB: 'lib'
};

var ENV_JSON = {};
ENV_JSON[ENV.DEV] = 'env-dev.json';
ENV_JSON[ENV.STAGE] = 'env-stage.json';
ENV_JSON[ENV.PROD] = 'env-prod.json';
ENV_JSON[ENV.DRP] = 'env-drp.json';

var CONFIG_JSON = {};
CONFIG_JSON[ENV.DEV] = 'config-dev.json';
CONFIG_JSON[ENV.STAGE] = 'config-stage.json';
CONFIG_JSON[ENV.PROD] = 'config-prod.json';
CONFIG_JSON[ENV.DRP] = 'config-drp.json';

module.exports = {
  DIR: DIR,
  TASK: TASK,
  UNIT_TEST_TOKENS: UNIT_TEST_TOKENS,
  ENV: ENV,
  ENV_JSON: ENV_JSON,
  CONFIG_JSON: CONFIG_JSON
};
