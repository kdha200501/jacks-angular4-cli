/*
global require, module
*/

// Global methods
var joinPath = require('path').join;

// Global variables
var TASK = require('./GulpConst').TASK;
var DIR = require('./GulpConst').DIR;

// maps CommonJs packages' import path to header file
var CommonJsHeaderFactory = function() {// closure
  'use strict';

  // private variables
  var moduleList = [{// client browser dependencies
    name: 'main',
    config: {defaultExtension: 'js'}
  }, {
    name: 'rxjs',
    config: {defaultExtension: 'js'}
  }, {
    name: 'angular-webstorage-service',
    config: {defaultExtension: 'js'}
  }, {
    name: 'mock-reponse',
    config: {defaultExtension: 'js', main: 'index.js'}
  }];

  var testModuleList = [{// client browser dependencies
    name: 'lodash',
    config: {defaultExtension: 'js'}
  }, {
    name: 'jacks-angular4-test-utils',
    config: {defaultExtension: 'js', main: 'index.js'}
  }];

  // public methods
  return {
    instantiate: function(task) {
      switch (task) {
        case TASK.BUILD:

          return moduleList;

          break;
        case TASK.UNIT_TEST:

          return [].concat(moduleList, testModuleList);

          break;
        default:

          return [];
      }
    },
    resolveHeader: function(op, module) {
      op[module.name] = module.config;
      return op;
    },
    getPathGlob: function(op, module) {
      if(module.name === 'main') {
        op.push(module.name + '.js');
      }
      else {
        op.push( joinPath(DIR.NODE_MODULES, module.name, '**', '*.*') );
      }
      return op;
    }
  };
}();// endClosure

module.exports = CommonJsHeaderFactory;
