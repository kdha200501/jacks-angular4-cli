/*
global require, module
*/

// Global methods
var joinPath = require('path').join;

// Global variables
var DIR = require('./GulpConst').DIR;
var ENV = require('./GulpConst').ENV;
var TASK = require('./GulpConst').TASK;

// maps Es2015 packages' import path to file path
var Es2015JsFactory = function() {// closure
  'use strict';

  // private variables and methods
  var sharedModuleList = [{
    name: 'zone.js',
    path: 'zone.js/dist/zone.min.js'
  }, {
    name: 'reflect-metadata',
    path: 'reflect-metadata/Reflect.js'
  }, {
    name: 'systemjs',
    path: 'systemjs/dist/system.js'
  }];

  var moduleList = [].concat(// client browser dependencies

    [{
      name: 'core-js',
      path: 'core-js/client/shim.min.js'
    }],

    sharedModuleList
  );

  var testModuleList = [].concat(// Karma browser dependencies
    [{
      name: 'es6-shim',
      path: 'es6-shim/es6-shim.min.js'
    }, {
      name: 'system-polyfills',
      path: 'systemjs/dist/system-polyfills.js'
    }, {
      name: 'babel-polyfill',
      path: 'babel-polyfill/dist/polyfill.min.js'
    }],

    sharedModuleList,

    [{
      name: 'zone-proxy',
      path: 'zone.js/dist/proxy.min.js'
    }, {
      name: 'zone-sync-test',
      path: 'zone.js/dist/sync-test.js'
    }, {
      name: 'zone-jasmine-patch',
      path: 'zone.js/dist/jasmine-patch.min.js'
    }, {
      name: 'zone-async-test',
      path: 'zone.js/dist/async-test.js'
    }, {
      name: 'zone-fake-async-test',
      path: 'zone.js/dist/fake-async-test.js'
    }]
  );

  // public methods
  return {
    instantiate: function(task) {
      switch (task) {
        case TASK.BUILD:

          return moduleList;

        break;
        case TASK.UNIT_TEST:

          return testModuleList;

        break;
        default:

          return [];
      }
    },
    resolvePath: function(op, module) {
      op.push( joinPath(DIR.NODE_MODULES, module.path) );
      return op;
    }
  };
}();// endClosure

module.exports = Es2015JsFactory;
