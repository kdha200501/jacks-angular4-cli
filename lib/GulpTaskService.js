/*
global require, module
*/

// Global methods
var registerTask = require('gulp').task;
var chainTask = require('gulp').series;
var convergeTask = require('gulp').parallel;
var gulp = require('gulp');
var safeAccess = require('safe-access');
var tsc = require('gulp-typescript');
var ngc = require('@angular/compiler-cli/src/main').main;
var webpack = require('webpack');
var joinPath = require('path').join;
var dirname = require('path').dirname;
var parseUrl = require('url').parse;
var map = require('map-stream');
var gulpIf = require('gulp-if');
var merge = require('merge-stream');
var watch = require('gulp-watch');
var parseKarmaConfig = require('karma/lib/config').parseConfig;

// Global Classes
var NodePackageService = require('./NodePackageService');
var VinylService = require('./VinylService');
var BrowserService = require('./BrowserService');
var LogService = require('./LogService');
var Es2015JsFactory = require('./Es2015JsFactory');
var CommonJsHeaderFactory = require('./CommonJsHeaderFactory');
var CommonJsFactory = require('./CommonJsFactory');
var UnitTestServer = require('karma').Server;

// Global variables
var DIR = require('./GulpConst').DIR;
var TASK = require('./GulpConst').TASK;
var URL = require('./GulpArgv').URL;
var BUILD = require('./GulpArgv').BUILD;

/*
    note:
        gulp.src() works with paths relative to process.cwd()
        require() works with paths relative to this file's __dirname
*/

var GulpTaskService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageService = NodePackageService.getInstance();
  var vinylService = VinylService.getInstance();

  var gulpTaskList = [{

    name: 'distRmRF',
    method: function() {
      return nodePackageService.rmRF(DIR.DIST);
    }

  }, {

    name: 'docRmRF',
    method: function() {
      return nodePackageService.rmRF(DIR.DOC);
    }

  }, {

    name: 'distCleanAot',
    method: function() {
      return nodePackageService.rmRF([
        joinPath(DIR.DIST, '*'),
        joinPath('!' + DIR.DIST, '*.min.js'),
        joinPath('!' + DIR.DIST, '*.html')
      ]);
    }

  }, {

    name: 'distCleanLibrary',
    method: function() {
      return nodePackageService.rmRF([
        joinPath(DIR.DIST, '*'),
        joinPath('!' + DIR.DIST, DIR.LIB)
      ]);
    }

  }, {

    name: 'distMkdirP',
    method: function(next) {
      return nodePackageService.mkdirP(DIR.DIST, function(err) {
        if(err === null) {
          next();
        }
        else {
          throw err;
        }
      });
    }

  }, {

    name: 'docMkdirP',
    method: function(next) {
      return nodePackageService.mkdirP(DIR.DOC, function(err) {
        if(err === null) {
          next();
        }
        else {
          throw err;
        }
      });
    }

  }, {

    name: 'tsLint',
    method: function() {
      var src = gulp.src( joinPath(DIR.SRC, '**', '*.ts'), {allowEmpty: true} );
      var tslintConfigFilePath = joinPath(__dirname, '..', 'tslint.json');
      return src
        .pipe( nodePackageService.tslint({
          formatter: 'verbose',
          configuration: tslintConfigFilePath
        }) )
        .pipe( nodePackageService.tslintReport({
          emitError: false,
          summarizeFailureOutput: true
        }) )
        .pipe( map(vinylService.stopStreamOnTsLintError) );
    }

  }, {

    name: 'injectEnvConst',
    method: function() {
      var src = gulp.src( joinPath(DIR.SRC, '**', 'env.ts'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      return src
        .pipe( map(vinylService.injectEnvConst.inject) )
        .pipe(dst)
    }

  }, {

    name: 'compileEnvConst',
    method: function() {
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-jit.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var src = gulp.src( joinPath(DIR.DIST, DIR.SRC, '**', 'env.ts'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );

      return src
        .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
        .pipe( tsc(tscConfig.compilerOptions) )
        .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
        .pipe(dst);
    }

  }, {

    name: 'injectConfigConst',
    method: function() {
      var src = gulp.src( joinPath(DIR.SRC, '**', 'config.ts'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      return src
        .pipe( map(vinylService.injectConfigConst.inject) )
        .pipe(dst);
    }

  }, {

    name: 'compileConfigConst',
    method: function() {
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-jit.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var src = gulp.src( joinPath(DIR.DIST, DIR.SRC, '**', 'config.ts'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );

      return src
        .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
        .pipe( tsc(tscConfig.compilerOptions) )
        .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
        .pipe(dst);
    }

  }, {

    name: 'watchConstant',
    method: function(next, logInfo) {
      var watchConfig = {ignoreInitial: true};
      var watcher = gulp.watch([// note, this task will get called per each changed file,
        // not ideal, can make a service to debounce
        joinPath(DIR.SRC, '**', vinylService.injectEnvConst.envJson),
        joinPath(DIR.SRC, '**', vinylService.injectConfigConst.configJson)
      ], watchConfig, chainTask(
        convergeTask(
          'injectEnvConst',
          'injectConfigConst'
        ),
        'compileEnvConst',
        'compileConfigConst',
        BrowserService.reload
      ));

      watcher.on('change', function(path) {
        logInfo('compile constant file:', path);
      });
    }

  }, {

    name: 'copySource',
    method: function() {
      var src = gulp.src([
        joinPath(DIR.SRC, '**', '*.ts'),
        joinPath('!' + DIR.SRC, '**', 'env.ts'),
        joinPath('!' + DIR.SRC, '**', 'config.ts')
      ], {allowEmpty: true});
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      return src.pipe(dst);
    }

  }, {

    name: 'renameLazyloadModule',
    method: function() {// closure

      // private variables and methods
      var regexpUrlHref = /es6-promise-loader\?,\[name\]!(.*)/;
      var regexpUrlHash = /^#(.*)/;
      var testUrlHref = function(route) {
        return regexpUrlHref.test(route.loadChildren);
      };
      var getUrlHref = function(route) {
        return parseUrl( route.loadChildren.replace(regexpUrlHref, '$1') );
      };
      var getDefaultClassName = function(url) {
        return url.hash.replace(regexpUrlHash, '$1')
      };

      // public method
      return function(next, logInfo, logError) {
        var routeConfig = require(
          joinPath(DIR.CWD, DIR.SRC, DIR.ROOT_APP, vinylService.injectConfigConst.configJson)
        );
        var urlList = (safeAccess(routeConfig, 'routes') || [])
          .filter(testUrlHref)
          .map(getUrlHref);

        if(!urlList[0]) {
          logInfo('lazyloaded modules not found');
          next();
          return;
        }

        var pipeList = urlList.map(function(url) {
          var from = url.pathname;
          from = joinPath.apply( null, from.split('/') );// convert url path to OS specific file path
          from = joinPath( DIR.DIST, DIR.SRC, DIR.ROOT_APP, dirname(from), 'AppModule.ts' );
          var to = [getDefaultClassName(url), 'ts'].join('.');

          if( !nodePackageService.fileExists(from) ) {
            logError('file not found:', from);
            return;
          }

          BUILD.LAZYLOAD_MODULE.push(from);
          logInfo('rename file', from, 'to', to);

          var src = gulp.src(from, {allowEmpty: true});
          var dst = gulp.dest( dirname(from) );

          return src
            .pipe( nodePackageService.renameFile(to) )
            .pipe(dst)
        });

        return merge.apply(null, pipeList);
      }

    }()// endClosure

  }, {

    name: 'cleanLazyloadModule',
    method: function() {
      return nodePackageService.rmRF(BUILD.LAZYLOAD_MODULE);
    }

  }, {

    name: 'compileSass',
    method: function() {
      var isDev = nodePackageService.getArgs().dev;
      var src = gulp.src( joinPath(DIR.SASS, '**', '*.scss'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.CSS) );

      return src
        .pipe(
          nodePackageService.compileSass().on( 'error', nodePackageService.sassResport() )
        )
        .pipe( gulpIf(!isDev, nodePackageService.minifyCss()) )
        .pipe(dst);
    }

  }, {

    name: 'watchSass',
    method: function(next, logInfo) {

      var watchConfig = {ignoreInitial: true};
      return watch(joinPath(DIR.SASS, '**', '*.scss'), watchConfig, function(vinyl) {

        var isDev = nodePackageService.getArgs().dev;
        var src = gulp.src( joinPath(DIR.SASS, vinyl.relative), {allowEmpty: true} );
        var path = joinPath( DIR.DIST, DIR.CSS, dirname(vinyl.relative) );
        var dst = gulp.dest(path);
        logInfo('compile sass file:', vinyl.path);

        return src
          .pipe(
            nodePackageService.compileSass().on( 'error', nodePackageService.sassResport() )
          )
          .pipe( gulpIf(!isDev, nodePackageService.minifyCss()) )
          .pipe(dst)
          .pipe( map(BrowserService.reload) );

      });
    }

  }, {

    name: 'linkCssRecursive',
    method: function() {
      var srcDir = joinPath(DIR.DIST, DIR.CSS);
      var dstDir = joinPath(DIR.DIST, DIR.SRC);
      var pipeList = nodePackageService.ls(dstDir).reduce(function(op, item) {
        var filePath = joinPath(dstDir, item);
        var fileProperty = nodePackageService.fileProperty(filePath);
        if( fileProperty.isDirectory() ) {
          op.push(
            gulp.src(srcDir, {read: false, allowEmpty: true}).pipe( gulp.symlink(filePath) )
          );
        }
        return op;
      }, []);

      return merge.apply(null, pipeList);
    }

  }, {

    name: 'linkView',
    method: function() {
      var src = gulp.src(DIR.VIEW, {read: false, allowEmpty: true});
      var dst = gulp.symlink(DIR.DIST);
      return src.pipe(dst);
    }

  }, {

    name: 'watchView',
    method: function(next, logInfo) {
      var watchConfig = {ignoreInitial: true};
      gulp.watch( joinPath(DIR.VIEW, '**', '*.html'), watchConfig )
        .on('change', function(path) {
          logInfo('html file changed:', path);
          BrowserService.reload();
        });
    }

  }, {

    name: 'sanitizeView',
    method: function() {
      var src = gulp.src( joinPath(DIR.VIEW, '**', '*.html'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.VIEW) );

      return src
        .pipe( map(vinylService.minifyAndRemoveUnitTestAttributes) )
        .pipe(dst);
    }

  }, {

    name: 'linkViewRecursive',
    method: function() {
      var srcDir = joinPath(DIR.DIST, DIR.VIEW);
      var dstDir = joinPath(DIR.DIST, DIR.SRC);
      var pipeList = nodePackageService.ls(dstDir).reduce(function(op, item) {
        var filePath = joinPath(dstDir, item);
        var fileProperty = nodePackageService.fileProperty(filePath);
        if( fileProperty.isDirectory() ) {
          op.push(
            gulp.src(srcDir, {read: false, allowEmpty: true}).pipe( gulp.symlink(filePath) )
          );
        }
        return op;
      }, []);

      return merge.apply(null, pipeList);
    }

  }, {

    name: 'linkNodeModules',
    method: function() {
      var src = gulp.src(DIR.NODE_MODULES, {read: false, allowEmpty: true});
      var dst = gulp.symlink(DIR.DIST);
      return src.pipe(dst);
    }

  }, {

    name: 'compileJit',
    method: function() {
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-jit.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var src = gulp.src([
        joinPath(DIR.SRC, '**', '*.ts'),
        joinPath('!' + DIR.SRC, '**', 'env.ts'),
        joinPath('!' + DIR.SRC, '**', 'config.ts')
      ], {allowEmpty: true});
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      var dstCopy = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );

      return src
        .pipe(dstCopy)
        .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
        .pipe( tsc(tscConfig.compilerOptions) )
        .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
        .pipe(dst)
    }

  }, {

    name: 'watchSource',
    method: function(next, logInfo) {
      var tslintConfigFilePath = joinPath(__dirname, '..', 'tslint.json');
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-jit.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var watchConfig = {ignoreInitial: true};
      return watch([
        joinPath(DIR.SRC, '**', '*.ts'),
        joinPath('!' + DIR.SRC, '**', 'env.ts'),
        joinPath('!' + DIR.SRC, '**', 'config.ts')
      ], watchConfig, function(vinyl) {

        var src = gulp.src( joinPath(DIR.SRC, vinyl.relative), {allowEmpty: true} );
        var path = joinPath( DIR.DIST, DIR.SRC, dirname(vinyl.relative) );
        var dst = gulp.dest(path);
        var dstCopy = gulp.dest(path);
        logInfo('compile ts file:', vinyl.path);

        return src
          .pipe( nodePackageService.tslint({
            formatter: 'verbose',
            configuration: tslintConfigFilePath
          }) )
          .pipe( nodePackageService.tslintReport({
            emitError: false,
            summarizeFailureOutput: true
          }) )
          .pipe(dstCopy)
          .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
          .pipe( tsc(tscConfig.compilerOptions) )
          .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
          .pipe(dst)
          .pipe( map(BrowserService.reload) );

      });
    }

  }, {

    name: 'compileUnitTest',
    method: function() {
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-jit.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var src = gulp.src( joinPath(DIR.UNIT_TEST, '**', '*.ts'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.UNIT_TEST) );
      var dstCopy = gulp.dest( joinPath(DIR.DIST, DIR.UNIT_TEST) );

      return src
        .pipe(dstCopy)
        .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
        .pipe( tsc(tscConfig.compilerOptions) )
        .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
        .pipe(dst);
    }

  }, {

    name: 'compileLibrary',
    method: function() {
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-lib.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var src = gulp.src( joinPath(DIR.SRC, '**', '*.ts'), {allowEmpty: true} );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.LIB) );
      var dstCopy = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );

      return src
        .pipe(dstCopy)
        .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
        .pipe( tsc(tscConfig.compilerOptions) )
        .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
        .pipe(dst)
    }

  }, {

    name: 'copyTsConfigAot',
    method: function() {
      var src = gulp.src( joinPath(__dirname, '..', 'tsconfig-aot.json') );
      var dst = gulp.dest(DIR.DIST);
      return src
        .pipe( nodePackageService.renameFile('tsconfig.json') )
        .pipe(dst);
    }

  }, {

    name: 'copyTsConfigMetadata',
    method: function() {
      var src = gulp.src( joinPath(__dirname, '..', 'tsconfig-metadata.json') );
      var dst = gulp.dest(DIR.DIST);
      return src
        .pipe( map(vinylService.injectConstants) )
        .pipe( nodePackageService.renameFile('tsconfig.json') )
        .pipe(dst);
    }

  }, {

    name: 'copyMainJit',
    method: function() {
      var tscConfig = require( joinPath(__dirname, '..', 'tsconfig-jit.json') );
      var sourceMap = safeAccess(tscConfig, 'compilerOptions.sourceMap' ) === true;

      var src = gulp.src( joinPath(__dirname, '..', 'main-jit.ts') );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      var dstCopy = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      return src
        .pipe( map(vinylService.injectConstants) )
        .pipe( nodePackageService.renameFile('main.ts') )
        .pipe(dstCopy)
        .pipe( gulpIf(sourceMap, nodePackageService.createSourceMap()) )
        .pipe( tsc(tscConfig.compilerOptions) )
        .pipe( gulpIf(sourceMap, nodePackageService.writeSourceMap('.')) )
        .pipe(dst)
    }

  }, {

    name: 'copyMainAot',
    method: function() {
      var src = gulp.src( joinPath(__dirname, '..', 'main-aot.ts') );
      var dst = gulp.dest( joinPath(DIR.DIST, DIR.SRC) );
      return src
        .pipe( map(vinylService.injectConstants) )
        .pipe( nodePackageService.renameFile('main.ts') )
        .pipe(dst);
    }

  }, {

    name: 'copySystemjsConfig',
    method: function() {
      var mapCollection = {main: '/' + DIR.SRC};
      mapCollection = CommonJsFactory.instantiate(TASK.BUILD)
        .reduce(CommonJsFactory.resolvePath, mapCollection);
      mapCollection['mock-reponse'] = joinPath(DIR.MOCK_SERVER, DIR.ROUTE_API);

      var packageCollection = {};
      packageCollection = CommonJsHeaderFactory.instantiate(TASK.BUILD)
        .reduce(CommonJsHeaderFactory.resolveHeader, packageCollection);

      var variableConfig = {
        '__configStr': JSON.stringify({map: mapCollection, packages: packageCollection}),
        '__srcDir': DIR.SRC
      };

      var src = gulp.src( joinPath(__dirname, '..', 'config.systemjs.js') );
      var dst = gulp.dest(DIR.DIST);

      return src
        .pipe( nodePackageService.injectVariables(variableConfig) )
        .pipe(dst);
    }

  }, {

    name: 'copyWebpackConfig',
    method: function() {
      var src = gulp.src( joinPath(__dirname, '..', 'config.webpack.js') );
      var dst = gulp.dest(DIR.DIST);
      return src
        .pipe( map(vinylService.injectConstants) )
        .pipe(dst);
    }

  }, {

    name: 'copyKarmaConfig',
    method: function() {
      var pipeList = [];

      var srcKarmaConfig = gulp.src( joinPath(__dirname, '..', 'config.karma.js') );
      var dstKarmaConfig = gulp.dest(DIR.DIST);
      pipeList.push(
        srcKarmaConfig
          .pipe( map(vinylService.injectConstants) )
          .pipe(dstKarmaConfig)
      );

      var mapCollection = {main: ''};
      mapCollection = CommonJsFactory.instantiate(TASK.UNIT_TEST)
        .reduce(CommonJsFactory.resolvePath, mapCollection);
      mapCollection['mock-reponse'] = joinPath(DIR.MOCK_SERVER, DIR.ROUTE_API);

      var packageCollection = {};
      packageCollection = CommonJsHeaderFactory.instantiate(TASK.UNIT_TEST)
        .reduce(CommonJsHeaderFactory.resolveHeader, packageCollection);

      var variableConfig = {
        '__configStr': JSON.stringify({map: mapCollection, packages: packageCollection})
      };

      var srcKarmaMain = gulp.src( joinPath(__dirname, '..', 'main-unit-test.js') );
      var dstKarmaMain = gulp.dest(DIR.DIST);

      pipeList.push(
        srcKarmaMain
          .pipe( map(vinylService.injectConstants) )
          .pipe( nodePackageService.injectVariables(variableConfig) )
          .pipe( nodePackageService.renameFile('main.js') )
          .pipe(dstKarmaMain)
      );

      return merge.apply(null, pipeList);
    }

  }, {

    name: 'compileAot',
    method: function(next, logInfo, logError) {
      var ngcConfig = {
        project: joinPath(DIR.CWD, DIR.DIST, 'tsconfig.json')
      };
      ngc(ngcConfig).then(function(exitCode) {
        if(exitCode === 0) {
          logInfo('ngc complete');
          next();
        }
        else {
          logError('ngc exited with error');
          exit()
        }
      }, function() {
        logError('ngc exited with error');
        exit();
      });
    }

  }, {

    name: 'copyIndexHtmlJit',
    method: function() {
      var src = gulp.src( joinPath(__dirname, '..', 'index-jit.html'), {allowEmpty: true} );
      var dst = gulp.dest(DIR.DIST);
      var srcModule = gulp.src(
        Es2015JsFactory.instantiate(TASK.BUILD).reduce(Es2015JsFactory.resolvePath, []),
        {read: false}
      );
      return src
        .pipe( nodePackageService.injectModule(srcModule, {addRootSlash: true}) )
        .pipe( nodePackageService.renameFile('index.html') )
        .pipe(dst);
    }

  }, {

    name: 'copyIndexHtmlAot',
    method: function() {
      var src = gulp.src( joinPath(__dirname, '..', 'index-aot.html') );
      var dst = gulp.dest(DIR.DIST);
      var srcBuild = gulp.src( joinPath(DIR.DIST, 'build*.min.js'), {read: false} );
      var injectConfig = {name: 'build', relative: false, ignorePath: DIR.DIST, addRootSlash: false, removeTags: true};
      return src
        .pipe( nodePackageService.injectModule(srcBuild, injectConfig) )
        .pipe( nodePackageService.renameFile('index.html') )
        .pipe(dst);
    }

  }, {

    name: 'codeSplit',
    method: function(next, logInfo, logError) {
      var webpackConfig = require( joinPath(DIR.CWD, DIR.DIST, 'config.webpack.js') );
      webpack(webpackConfig, function(error/*, status*/) {
        if(error) {
          logError('webpack error:', error);
          exit();
        }
        logInfo('webpack complete');
        next();
      });
    }

  }, {

    name: 'startServer',
    method: function() {// closure

      // private variables
      var redirectRequestWithoutSuffixToSPA = '^/([^\\.]*)$ /index.html [L]';

      // public method
      return function(next, logInfo) {
        var serverConfig = {
          port: 3000,
          server: {
            baseDir: DIR.DIST,
            middleware: [
              nodePackageService.rewriteRequest([redirectRequestWithoutSuffixToSPA])
            ]
          },
          ui: false,
          open: !URL.BUILD
        };

        nodePackageService.startServer(serverConfig, function(err) {
          if(err) {
            throw err;
          }
          if(URL.BUILD) {
            logInfo('opening URL:', URL.BUILD);
            nodePackageService.openUrl(URL.BUILD);
          }
          next();
        });
      }

    }()// endClosure
  }, {

    name: 'startUnitTestServer',
    method: function() {// closure
      // private variables and methods
      var globToPattern = function (glob) {
        return glob === 'main.js' ?
          {pattern: glob, included: true} :
          {pattern: glob, included: false, served: true, nocache: false};
      };

      // public method
      return function(next) {
        var karmaConfig = parseKarmaConfig( joinPath(DIR.CWD, DIR.DIST, 'config.karma.js'), {} );
        karmaConfig.singleRun = !nodePackageService.getArgs().watch;
        karmaConfig.files = []
          .concat(
            Es2015JsFactory.instantiate(TASK.UNIT_TEST)
              .reduce(Es2015JsFactory.resolvePath, []),
            CommonJsFactory.instantiate(TASK.UNIT_TEST)
              .reduce(CommonJsFactory.getPathGlob, [])
              .map(globToPattern),
            CommonJsHeaderFactory.instantiate(TASK.UNIT_TEST)
              .reduce(CommonJsHeaderFactory.getPathGlob, [])
              .map(globToPattern),
            karmaConfig.files
          );

        if(karmaConfig.singleRun === true) {
          new UnitTestServer(karmaConfig, next).start();
          return;
        }

        // next();

        // config.detached = true;// detached servers loose event listeners!
        // new UnitTestServer(config, next).start();
        //
        // var configRunner = karmaParseConfig( joinPath(DIR.CWD, DIR.DIST, 'config.karma.js'), {} );
        // configRunner.files = config.files;
        // var runUnitTest = function () {
        //   runner.run(configRunner, noop);
        // };
        // TODO: watch for TDD
      }
    }()// endClosure

  }, {

    name: 'stopUnitTestServer',
    method: function(next, logInfo, logError) {
      var cmd = [
        joinPath(DIR.CWD, DIR.NODE_MODULES, 'karma', 'bin', 'karma'),
        'stop'
      ].join(' ');

      nodePackageService.eval(cmd, function (/*err, stdout, stderr*/) {
        next();
      });
    }

  }, {

    name: 'linkMockServer',
    method: function() {
      var src = gulp.src(DIR.MOCK_SERVER, {read: false, allowEmpty: true});
      var dst = gulp.symlink(DIR.DIST);
      return src.pipe(dst);
    }

  }, {

    name: 'watchMockServerRouteApi',
    method: function(next, logInfo) {
      var watchConfig = {ignoreInitial: true};
      gulp.watch( joinPath(DIR.MOCK_SERVER, DIR.ROUTE_API, '*.{ts,js}'), watchConfig )
        .on('change', function(path) {
          logInfo('mock server route api changed:', path);
          BrowserService.reload();
        });
    }

  }, {

    name: 'generateDocumentation',
    method: function() {
      var src = gulp.src([
        joinPath(DIR.SRC, '**', '*.ts'),
        joinPath('!' + DIR.SRC, '**', 'env.ts'),
        joinPath('!' + DIR.SRC, '**', 'config.ts')
      ], {allowEmpty: true});
      var docConfig = {
        // TypeScript options (see typescript docs)
        target: 'es5',
        module: 'commonjs',
        includeDeclarations: true,

        // Output options (see typedoc docs)
        out: joinPath('.', DIR.DOC),

        // TypeDoc options (see typedoc docs)
        name: 'documentation',
        theme: 'default',
        ignoreCompilerErrors: true,
        experimentalDecorators: true,
        excludeExternals: true
      };

      return src.pipe( nodePackageService.document(docConfig) );
    }

  }, {

    name: 'noop',
    method: function(next) {
      next();
    }

  }];

  // private static class
  var singleton = null;
  var GulpTaskService_ = function(config) {
    return function(gulpTaskList) {// closure

      // private variables and methods
      var buildService = function(service, gulpTask) {
        var logInfo = function() {// prepended info log with task name
          LogService.info.apply(gulpTask.name, arguments);
        };
        var logError = function() {// prepended error log with task name
          LogService.error.apply(gulpTask.name, arguments);
        };
        var method = function(next) {
          if(gulpTask.name !== 'noop') {
            logInfo('Started');
          }
          return gulpTask.method(next, logInfo, logError);
        };
        registerTask(gulpTask.name, method);
        service[gulpTask.name] = method;
        return service;
      };

      // public methods
      return gulpTaskList.reduce(buildService, {});

    }(config.gulpTaskList || []);// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new GulpTaskService_({gulpTaskList: gulpTaskList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = GulpTaskService;
