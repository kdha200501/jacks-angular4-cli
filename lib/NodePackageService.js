/*
global require, module
*/

// Global methods
var safeAccess = require('safe-access');

// Global variables
var ENV = require('./GulpConst').ENV;

var NodePackageService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageList = [{
    name: 'yargs',
    config: function(nodePackage) {
      nodePackage.boolean([// force args to bool
        // environment flags
        ENV.DEV, ENV.STAGE, ENV.PROD, ENV.DRP, ENV.LIB,
        // build specific flags
        'serve', 'local',
        // unit test specific flags
        'watch'
      ]);
    },
    method: 'argv',
    serviceMethod: 'getArgs'
  }, {
    name: 'fs-extra',
    method: 'mkdirs()',
    serviceMethod: 'mkdirP'
  }, {
    name: 'fs-extra',
    method: 'copy()',
    serviceMethod: 'cpR'
  }, {
    name: 'fs-extra',
    method: 'readJsonSync()',
    serviceMethod: 'readJsonFile'
  }, {
    name: 'gulp-template',
    serviceMethod: 'injectVariables'
  }, {
    name: 'del',
    serviceMethod: 'rmRF'
  }, {
    name: 'del',
    serviceMethod: 'rm'
  }, {
    name: 'gulp-tslint',
    serviceMethod: 'tslint'
  }, {
    name: 'gulp-tslint',
    method: 'report()',
    serviceMethod: 'tslintReport'
  }, {
    name: 'browser-sync',
    config: function(nodePackage) {
      nodePackage.create();
    },
    method: 'init()',
    serviceMethod: 'startServer'
  }, {
    name: 'browser-sync',
    method: 'reload()',
    serviceMethod: 'reloadBrowser'
  }, {
    name: 'connect-modrewrite',
    serviceMethod: 'rewriteRequest'
  }, {
    name: 'openurl',
    method: 'open()',
    serviceMethod: 'openUrl'
  }, {
    name: 'fs',
    method: 'readdirSync()',
    serviceMethod: 'ls'
  }, {
    name: 'fs',
    method: 'lstatSync()',
    serviceMethod: 'fileProperty'
  }, {
    name: 'fs',
    method: 'ReadStream()',
    serviceMethod: 'readStream'
  }, {
    name: 'file-exists',
    method: 'sync()',
    serviceMethod: 'fileExists'
  }, {
    name: 'gulp-rename',
    serviceMethod: 'renameFile'
  }, {
    name: 'gulp-sass',
    serviceMethod: 'compileSass'
  }, {
    name: 'gulp-sass',
    method: 'logError',
    serviceMethod: 'sassResport'
  }, {
    name: 'gulp-uglifycss',
    serviceMethod: 'minifyCss'
  }, {
    name: 'gulp-inject',
    serviceMethod: 'injectModule'
  }, {
    name: 'gulp-sourcemaps',
    method: 'init()',
    serviceMethod: 'createSourceMap'
  }, {
    name: 'gulp-sourcemaps',
    method: 'write()',
    serviceMethod: 'writeSourceMap'
  }, {
    name: 'url',
    method: 'parse()',
    serviceMethod: 'parseUrl'
  }, {
    name: 'child_process',
    method: 'exec()',
    serviceMethod: 'eval'
  }, {
    name: 'gulp-typedoc',
    serviceMethod: 'document'
  }];

  // private static class
  var singleton = null;
  var NodePackageService_ = function(config) {
    return function(nodePackageList) {// closure

      // private variables and methods
      var buildService = function(service, nodePackage) {
        var node = require(nodePackage.name);
        // 1. config node package
        if( (typeof nodePackage.config).toLowerCase() === 'function' ) {
          nodePackage.config(node);
        }
        // 2. expose node package method
        if( (typeof nodePackage.method).toLowerCase() === 'string' ) {
          service[nodePackage.serviceMethod] = function() {
            return safeAccess( node, nodePackage.method, Array.prototype.slice.call(arguments, 0) );
          };
        }
        else {
          service[nodePackage.serviceMethod] = node;
        }
        return service;
      };

      // public methods
      return nodePackageList.reduce(buildService, {});

    }(config.nodePackageList || []);// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new NodePackageService_({nodePackageList: nodePackageList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = NodePackageService;
