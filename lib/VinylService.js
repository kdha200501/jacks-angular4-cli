/*
global require, module
*/

// Global methods
var exit = require('gulp-exit');
var safeAccess = require('safe-access');
var joinPath = require('path').join;
var dirname = require('path').dirname;

// Global constants
var CONFIG_JSON = require('./GulpConst').CONFIG_JSON;
var ENV_JSON = require('./GulpConst').ENV_JSON;
var ENV = require('./GulpConst').ENV;
var DIR = require('./GulpConst').DIR;
var UNIT_TEST_TOKENS = require('./GulpConst').UNIT_TEST_TOKENS;

// Global Classes
var NodePackageService = require('./NodePackageService');
var LogService = require('./LogService');
var Cheerio = require('cheerio');

var VinylService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageService = NodePackageService.getInstance();
  
  var vinylMethodList = [{
    
    name: 'stopStreamOnTsLintError',
    method: function(vinyl, next){
      if( safeAccess(vinyl, 'tslint.errorCount') === 0 ) {
        next(null, vinyl);
      }
      else {
        LogService.error.call('tsLint', 'process is terminated due to error in:', vinyl.path);
        exit();
      }
    }
    
  }, {

    name: 'injectEnvConst',
    method: function() {// closure

      // private methods
      var getEnv = function(config) {
        var env = null;
        for(var key in ENV_JSON) {
          if( ENV_JSON.hasOwnProperty(key) && config[key] === true ) {
            if( env !== ENV.DEV && nodePackageService.getArgs().local === true ) {
              env = ENV.DEV;
            }// endIf use local environment variables in non dev builds
            else {
              env = key;
            }
            break;
          }
        }
        return env;
      };

      var envJson = ENV_JSON[getEnv( nodePackageService.getArgs() )];

      // public variables and methods
      return {
        envJson: envJson,
        inject: function(vinyl, next) {
          var pathEnvConfig = joinPath( dirname(vinyl.path), envJson );
          var envConfig = nodePackageService.fileExists(pathEnvConfig) ?
            nodePackageService.readJsonFile(pathEnvConfig) : {};
          envConfig.DEV = nodePackageService.getArgs().dev;
          envConfig.STAGE = nodePackageService.getArgs().stage;
          envConfig.PROD = nodePackageService.getArgs().prod;
          envConfig.DRP = nodePackageService.getArgs().drp;
          envConfig.LOCAL = nodePackageService.getArgs().local;

          var envConfigStr = vinyl.contents
            .toString()
            .replace( /export const ENV = \{\}\;/, 'export const ENV = ' + JSON.stringify(envConfig) + ';' );
          vinyl.contents = Buffer.from(envConfigStr);

          next(null, vinyl);
        }
      };

    }()// endClosure

  }, {

    name: 'injectConfigConst',
    method: function() {// closure

      // private methods
      var getEnv = function(config) {
        var env = null;
        for(var key in CONFIG_JSON) {
          if( CONFIG_JSON.hasOwnProperty(key) && config[key] === true ) {
            env = key;
            break;
          }
        }
        return env;
      };

      var configJson = CONFIG_JSON[getEnv( nodePackageService.getArgs() )];

      // public method
      return {
        configJson: configJson,
        inject: function(vinyl, next) {
          var pathConfig = joinPath( dirname(vinyl.path), configJson );
          var config = nodePackageService.fileExists(pathConfig) ? nodePackageService.readJsonFile(pathConfig) : {};

          var configStr = vinyl.contents
            .toString()
            .replace( /export const CONFIG = \{\}\;/, 'export const CONFIG = ' + JSON.stringify(config) + ';' );
          vinyl.contents = Buffer.from(configStr);

          next(null, vinyl);
        }
      };

    }()// endClosure

  }, {

    name: 'injectConstants',
    method: function(vinyl, next) {
      var replaceStr = vinyl.contents
        .toString()
        .replace(/<%= __srcDir %>/g, DIR.SRC)
        .replace(/<%= __rootAppDir %>/g, DIR.ROOT_APP)
        .replace(/<%= __viewDir %>/g, DIR.VIEW)
        .replace(/<%= __cssDir %>/g, DIR.CSS)
        .replace(/<%= __assetDir %>/g, DIR.ASSETS)
        .replace(/<%= __aotDir %>/g, DIR.AOT)
        .replace(/<%= __unitTestDir %>/g, DIR.UNIT_TEST)
        .replace(/<%= __nodeModulesDir %>/g, DIR.NODE_MODULES)
        .replace( /<%= __mockServiceRouteApiDir %>/g, joinPath(DIR.MOCK_SERVER, DIR.ROUTE_API) )
        .replace(/<%= __cwdDir %>/g, DIR.CWD)
        .replace(/<%= __distDir %>/g, DIR.DIST)
        .replace(/<%= __libDir %>/g, DIR.LIB);
      vinyl.contents = Buffer.from(replaceStr);
      next(null, vinyl);
    }

  }, {

    name: 'minifyAndRemoveUnitTestAttributes',
    method: function(vinyl, next) {
      var cheerioConfig = {
        lowerCaseAttributeNames: false,
        decodeEntities: false
      };
      var $ = Cheerio.load(vinyl.contents, cheerioConfig);

      UNIT_TEST_TOKENS.forEach(function(unitTestToken) {
        $('[' + unitTestToken + ']').removeAttr(unitTestToken);
        var unitTestTokenBind = ['attr', unitTestToken].join('.');
        var unitTestTokenBindEscapped = ['attr', unitTestToken].join('\\.');
        $('[' + unitTestTokenBindEscapped + ']').removeAttr(unitTestTokenBind);
      });

      vinyl.contents = Buffer.from(
        $.html()
          .replace(/(\r\n\s+|\n\s+|\r\s+)/gm, '')
          .replace(/(\r\n|\n|\r)/gm, '')
      );

      next(null, vinyl);
    }

  }];

  // private static class
  var VinylService_ = function(config) {
    return function(vinylMethodList) {// closure

      // private variables and methods
      var buildService = function(service, vinylMethod) {
        service[vinylMethod.name] = vinylMethod.method;
        return service;
      };

      // public methods
      return vinylMethodList.reduce(buildService, {});

    }(config.vinylMethodList || []);// endClosure
  };

  // public singleton
  var singleton = null;
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new VinylService_({vinylMethodList: vinylMethodList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = VinylService;
