module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['intl-shim', 'jasmine'],


    // list of files / patterns to load in the browser
    files: [
      {pattern: '<%= __unitTestDir %>/**/*.{js, map}', included: false},
      {pattern: '<%= __srcDir %>/**/*.{js, map}', included: false},
      {pattern: '<%= __viewDir %>/**/*.html', included: false},
      {pattern: '<%= __cssDir %>/**/*.css', included: false},
      {pattern: '<%= __assetDir %>/**/*.*', included: false},
      {pattern: '<%= __nodeModulesDir %>/**/*.{js, map, css, html}', included: false},
      {pattern: '<%= __nodeModulesDir %>/Intl/locale-data/jsonp/en-CA.js', included: true},
      {pattern: '<%= __mockServiceRouteApiDir %>/**/*js', included: false}
    ],

    // proxied base paths
    proxies: {
      // required for component assests fetched by Angular's compiler
      "/<%= __viewDir %>/": "/base/<%= __viewDir %>/",
      "/<%= __cssDir %>/": "/base/<%= __cssDir %>/",
      "/<%= __assetDir %>/": "/base/<%= __assetDir %>/",
      "/<%= __nodeModulesDir %>/": "/base/<%= __assetDir %>/",
      "/<%= __mockServiceRouteApiDir %>/": "/base/<%= __mockServiceRouteApiDir %>/"
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],

    coverageReporter: {
      type : 'json',
      subdir : '.',
      file : 'coverage-final.json'
    },

    preprocessors: {
      '../<%= __distDir %>/<%= __srcDir %>/**/*.js': ['coverage']
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'/*, 'Chrome', 'Firefox', 'Safari'*/],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
};
