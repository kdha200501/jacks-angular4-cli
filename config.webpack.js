const LoaderOptionsPlugin = require('webpack').LoaderOptionsPlugin;
const UglifyJsPlugin = require('webpack').optimize.UglifyJsPlugin;
const path = require('path');

const babelOptions = {
  presets: [
    ['es2015', {'modules': false}]
  ]
};

module.exports = {
  resolve: {
    extensions: ['.js', '.ts']
  },
  module: {
    rules: [{
      test: /\.ts$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: babelOptions
      }
    }, {
      test: /\.ts$/,
      loader: ['angular2-load-children-loader', 'light-ts-loader']
    }],
    noParse: [
      /zone\.js\/dist/,
    ]
  },
  entry: {
    build: path.resolve('<%= __cwdDir %>', '<%= __distDir %>', '<%= __srcDir %>', 'main')
  },
  output: {
    path: path.resolve('<%= __cwdDir %>', '<%= __distDir %>'),
    publicPath: './',
    filename: '[name].[hash].min.js',// filename entry build
    chunkFilename: '[name].[chunkhash].chunk.min.js'// filename for split chunks
  },
  plugins: [
    new LoaderOptionsPlugin({
      tsConfigPath: path.resolve('<%= __cwdDir %>', '<%= __distDir %>', 'tsconfig.json'),
      minimize: true,
      debug: false
    }),
    new UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      },
      sourceMap: true
    })
  ],
  devtool: 'sourcemap'
};
