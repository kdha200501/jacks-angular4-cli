/*
global jasmine, __karma__, window
*/

Error.stackTraceLimit = Infinity;
jasmine.DEFAULT_TIMEOUT_INTERVAL = 3000;

__karma__.loaded = function() {
};

var isSpecFile = function() {// closure
  // private variables and methods
  var regPath = new RegExp('^/base/<%= __unitTestDir %>/(.*)');
  var regJs = new RegExp('(.*).js$');
  // public method
  return function(path) {
    return regPath.test(path) && regJs.test(path);
  }
}();// endClosure

var allSpecFiles = Object.keys(window.__karma__.files)
  .filter(isSpecFile);

// Load our SystemJS configuration.
System.config({
  baseURL: '/base'
});

System.config( JSON.parse('<%= __configStr %>') );

Promise.all([
  System.import('@angular/core/testing'),
  System.import('@angular/platform-browser-dynamic/testing')
])
  .then(function(providers) {
    var testing = providers[0];
    var testingBrowser = providers[1];

    testing.TestBed.initTestEnvironment(
      testingBrowser.BrowserDynamicTestingModule,
      testingBrowser.platformBrowserDynamicTesting()
    );

  })
  .then(function() {
    // Finally, load all spec files.
    // This will run the tests directly.
    return Promise.all(
      allSpecFiles.map(function(moduleName) {
        return System.import(moduleName);
      })
    );
  })
  .then(__karma__.start, __karma__.error);
