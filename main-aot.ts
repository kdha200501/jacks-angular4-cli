import 'core-js/es6';
import 'core-js/es7/reflect';
import 'zone.js/dist/zone';

import {enableProdMode} from '@angular/core';
import {platformBrowser} from '@angular/platform-browser';
import {AppModuleNgFactory} from './<%= __rootAppDir %>/AppModule.ngfactory';

enableProdMode();
platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
